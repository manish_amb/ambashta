<?php 

namespace App\Http\Controllers;

class WelcomeController extends Controller{
	/**
	 * Instantiate few requisite properties
	 */
	public function __construct(){

	}

	public function welcome(){
		return view('welcome');
	}
}