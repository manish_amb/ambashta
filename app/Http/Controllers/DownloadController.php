<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DownloadController extends Controller
{
    //download script to client 
    public function myCV(){
    	$file = public_path() . "/download/Manish_Kumar_Concise.pdf";
    	$header = array("content-type"=>"application/pdf");

    	return response()->download($file,"Manish_Kumar",$header);

    } 
}
