@extends('layouts.master')

@section('title')
Welcome
@stop
@section('content')
<div id="content">
	<section id="navigation">
		<div class="container nav-box">
			<div class="row">
				<div class="img-box">
					<img src="{{url('images/profile.jpeg')}}" alt="">
				</div>
				<ul class="col-md-12 nav-link">
					<li><a href="#">Start</a></li>
					<li><a href="#">Resume</a></li>
					<li><a href="#">Portfolio</a></li>
					<li><a href="#">Testimonials</a></li>
					<li><a href="#">Articles</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="col-md-12 social-presence">
					<div class="col-md-12">My presence on internet</div>
					<div class="row social-icons">
						<div class="col-md-3 col"><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></div>
						<div class="col-md-3"><a href="https://www.twitter.com"><i class="fa fa-twitter"></i></a></div>
						<div class="col-md-3"><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></div>
						<div class="col-md-3"><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<section id="start" class="dark-theme">
		<div class="container-fluid section">
			<div class="row heading-box">
				<div class="col-md-12 heading-title">
					Hi! I'm <span>Manish Ambashta</span>
				</div>
				<div class="col-md-12 heading-details">
					Senior Software Developer, TheStorypedia
				</div>
				<a href="/my/cv" target="_blank">
					<div id="download-cv" class="col-md-2 download-cv">
						Download My CV
					</div>
				</a>
			</div>
		</div>
	</section>
	<section id="resume">
		<div class="container section">
			<div class="row resume-box">
				<div class="col-md-4">
					<div class="head-icon"><i class="fa fa-commenting-o"></i></div>
					<h3 class="sec1-title">About Me</h3>
					<h4><i>With 3 years of web development experience in an agile work-frame I have advanced from a Software Developer to Full Stack Developer. Code has become my passion, and to learn and advance as a coder is what I do in my free time.</i></h4>
					<div class="head-icon"><i class="fa fa-trophy"></i></div>
					<h3 class="sec2-title">Skills & Abilities</h3>
					<h5>Web Development has been my domain from the very begining, I have advanced my skills to become a Full Stack Developer who is not only expert in writing codes but manages whole architecture of the project, and handles the project from the start till end. I have worked in Agile Methodology</h5>
					<ul class="technical-skill-details">
						<li><i>Back End</i> : <b>Php, Python, NodeJs</b></li>
						<li><i>Databases</i> : <b>MySQL, MongoDB</b></li>
						<li><i>Front End</i> : <b>HTML5, CSS3, Javascript</b></li>
						<li><i>Plugins</i> : <b>Chrome, Firefox, Opera</b></li>
						<li><i>OS </i>: <b>Linux, Windows</b></li>
						<li><i>Other Tech Stacks</i> : <b>Jquery ,Bootstrap, RabbitMQ, Facebook JS SDK, Shell Scripts</b></li>
						<li><i>Framework</i> : <b>Laravel</b></li>
					</ul>	
				</div>
				<div class="col-md-4">
					<div class="head-icon"><i class="fa fa-calendar"></i></div>
					<h3 class="sec3-title">Jobs & Education</h3>
					<ul class="job-edu-details">
						<li>2014- 		: <b>Senior Software Developer</b> in <i>Shopzoi E-Services Pvt Ltd</i></li>
						<li>2013-2014   : <b>Software Developer</b> in <i>Zecross Media Pvt Ltd</i></li>
						<li>2009-2013   : <b>B.Tech Computer Science & Engineering</b> from <i>Heritage Institute of Technology, Kolkata</i> with <b>8.12 CGPA</b></li>
						<li>2008 		: Passed 12th from <i>Kendriya Vidyalaya</i> with <b>88.6%</b> marks</li>
						<li>2006 		: Passed 10th from <i>Kendriya Vidyalaya</i> with <b>88.2%</b> marks</li>
					</ul>	
				</div>
				<div class="col-md-4">
					<div class="head-icon"><i class="fa fa-music"></i></div>
					<h3>Extra Curriculars</h3>
					<h5>In my school days I have participated in plenty activities diverisified profusely from sports, quizzes, debates, songs, drawings, essays to martial arts.</h5>
					<ul class="extra-curr-details">
						<li>Gold Medal in 100m sprint</li>
						<li>Gold Medal in Javelin throw</li>
						<li>Gold Medal in Shotput</li>
						<li>Gold Medal in Hindi Essay</li>
						<li>Silver Medal in English Debate</li>
						<li>Gold Medal in Social Science Quiz</li>
						<li>Winner in Kabbaddi</li>
						<li>Winner in Volleyball</li>
						<li>Participated in Regional level Social Science Quiz</li>
						<li>Participated in Regional level Kho-Kho Competition</li>
						<!-- <li>Member of Rotaract Club of Heritage Institute of Technology</li> -->
					</ul>	
				</div>
			</div>
		</div>
	</section>
	<section id="portfolio">
		<div class="container section">
			<div class="row portfolio-box">
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/intellogrid.png')}}" alt="Intellogrid">
						<a target="_blank" href="http://www.intellogrid.com"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">Intellogrid – Ad Serving Network</div>
					<div class="project-desc">
						An Ad Content Exchange Service  that provides Reporting App for advertisers’ and publishers’ reports and a Widget App for serving ads on publishers’ websites.
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/thestorypedia.png')}}" alt="thestorypedia">
						<a target="_blank" href="http://www.thestorypedia.com"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">TheStorypedia</div>
					<div class="project-desc">
						An Entertainment News Website blogging interesting tales.
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/pushingo.png')}}" alt="pushingo">
						<a target="_blank" href="https://www.pushingo.com"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">Pushnigo</div>
					<div class="project-desc">
						A Web Service to send Web Push Notification.
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/couponzoi.png')}}" alt="coupons-shopzoi">
						<a target="_blank" href="https://coupons.shopzoi.com"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">Coupon Shopzoi</div>
					<div class="project-desc">
						A Coupons and Deals Website for eCommerce Websites
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/shopzoi.png')}}" alt="shopzoi">
						<a target="_blank" href="https://www.shopzoi.com"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">Shopzoi</div>
					<div class="project-desc">
						A Website to show real time Comparative Price of any product across popular eCommerce websites.
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/inbooster.png')}}" alt="Inbooster">
						<a target="_blank" href="https://chrome.google.com/webstore/detail/linkedin-booster-inbooste/cbeeippdidncnpajbnaoojahppclccha"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">InBooster</div>
					<div class="project-desc">
						A LinkedIn connection revealer, bookmark and show profile history.
					</div>
				</div>
				<div class="col-md-3 col-xs-6 project">
					<div class="project-img">
						<img src="{{url('images/shopzoi-extension.png')}}" alt="Shopzoi Chrome Extension">
						<a target="_blank" href="https://chrome.google.com/webstore/detail/shopzoi-the-best-price-co/oonmbhlklekhdngalpjnbpnjaodocfni"><div class="overlay-link"><i class="fa fa-link"></i></div></a>
					</div>
					<div class="project-title">Shopzoi Extension</div>
					<div class="project-desc">
						Shopzoi Extension is an online shopping search Assistant
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials">
		<div class="container section">
			<div class="row testimonials-box">
				<div class="col-md-12 experience-head">n years of experience.</div>
				<div class="col-md-12 testimonials-caption">This is what people I have worked with are saying.</div>
				<div class="col-md-4">
					<div class="comment">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess.
					</div>
					<div class="col-md-4 ref-profile-img">
						<img src="{{url('images/profile.jpeg')}}" alt="testimonial-1">
					</div>
					<div class="col-md-8 ref-text">
						<div class="ref-name">Mr. Red</div>
						<div class="ref-designation">Performer</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="comment">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess.
					</div>
					<div class="col-md-4 ref-profile-img">
						<img src="{{url('images/profile.jpeg')}}" alt="testimonial-1">
					</div>
					<div class="col-md-8 ref-text">
						<div class="ref-name">Mr. Red</div>
						<div class="ref-designation">Performer</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="comment">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess.
					</div>
					<div class="col-md-4 ref-profile-img">
						<img src="{{url('images/profile.jpeg')}}" alt="testimonial-1">
					</div>
					<div class="col-md-8 ref-text">
						<div class="ref-name">Mr. Red</div>
						<div class="ref-designation">Performer</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="comment">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess.
					</div>
					<div class="col-md-4 ref-profile-img">
						<img src="{{url('images/profile.jpeg')}}" alt="testimonial-1">
					</div>
					<div class="col-md-8 ref-text">
						<div class="ref-name">Mr. Red</div>
						<div class="ref-designation">Performer</div>
					</div>
				</div>
				<div class="clear-fix"></div>
			</div>
		</div>
	</section>
	<section id="contact" class="bg-footer">
		<div class="container-fluid section">
			<div class="row contact-box">
				<div class="col-md-4 contact-form">
					<div class="contact-form-title">
						Leave your details below
						will revert ASAP.
					</div>
					<div class="form" name="contact-me">
						<input id="name" class="form-control" type="text" name="name" placeholder="Name">
						<input id="email" class="form-control" type="text" name="name" placeholder="Email">
						<textarea id="message" class="form-control" name="name" placeholder="Message"></textarea>
						<input type="submit" id="send-message" name="submit" class="btn btn-primary" value="Submit">
					</div>
					<div class="gen-info">
						<div>Or you can reach me at</div>
						<div class="mail"><i class="fa fa-comments"></i>abc@gmail.com</div>
						<div>As of now I live at</div>
						<div class="address"><i class="fa fa-map-marker"></i>Dwarka, New Delhi, India</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop
