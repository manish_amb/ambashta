<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{asset('/')}}">
				<img src="{{url('images/profile.jpeg')}}" alt="Brand logo" id="brand-logo">
			</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse navbar-responsive-collapse">
				
			<ul class="nav navbar-nav middle-nav">
				<li><a class="nav-text-clr navbarfont" href="#">Home</a></li>
				<li><a class="nav-text-clr navbarfont" href="#">Home</a></li>
				<li><a class="nav-text-clr navbarfont" href="#">Home</a></li>
				<li><a class="nav-text-clr navbarfont" href="#">Home</a></li>
				<li><a class="nav-text-clr navbarfont" href="#">Home</a></li>
			</ul>
		</div>
	</div>
</nav>