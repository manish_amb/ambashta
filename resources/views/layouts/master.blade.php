<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="{{url('css/bootstrap-theme.min.css')}}"> -->
    <link rel="stylesheet" href="{{url('font-awesome/css/font-awesome.min.css')}}">	
	<link rel="stylesheet" href="{{url('css/style.css')}}">
    @yield('styles')
</head>
<body>
    @yield('content')
    <!-- scripts section -->
    <!-- jquery -->
    <script   src="{{url('js/jquery.js')}}"></script>
    <script   src="{{url('js/jquery-ui.min.js')}}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script   src="{{url('js/script.js')}}"></script>
    @yield('scripts')
</body>
</html>